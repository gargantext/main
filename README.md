<div align="center"><img height="180" src="./images/logo.png"></div>
&nbsp;

# Gargantext Main - Infos and repositories

Gargantext is a Free Software, [GNU aGPLV3 and CECILL variant affero compliant licence for French
translation](https://gitlab.iscpif.fr/gargantext/haskell-gargantext/blob
/stable/LICENSE) with a [published
documentation](https://write.frame.gargantext.org/s/3ffd6c05af5b1c3f40f01e34dc02b547864b3613f323581ab39d8cbdaf4eb9ba#).

Gargantext is developed by a community:
- [Team](https://gitlab.iscpif.fr/groups/gargantext/-/group_members)

Its first scientific investigators are:
- [Architect and Project Manager, Human science background](https://gitlab.iscpif.fr/anoe)
- [Scientific advisor, Math background](https://gitlab.iscpif.fr/davidchavalarias)


Please cite GarganText with this BibTex reference:

```
@misc{gargantext,
  author = {Delano\¨{e} Alexandre, Chavalarias David},
  annote   = {All contributors: \url{https://gitlab.iscpif.fr/groups/gargantext/-/group_members}},
  organization = {CNRS, ISC-PIF (UAR3611)},
  title = {GarganText, collaborative and decentralized LibreWare},
  year = {2023},
  publisher = {Complex Systems Institutes of Paris \^{I}le de France},
  journal = {GarganText organization on ISCPIFs' GitLab repository},
  howpublished = {\url{https://gitlab.iscpif.fr/gargantext/main}},
  note = {Version Number 0.0.7},
}
```

## New Version of Gargantext (v4)

- [Backend instance repository](https://gitlab.iscpif.fr/gargantext/haskell-gargantext)
- [Frontend instance repository](https://gitlab.iscpif.fr/gargantext/purescript-gargantext)

## Previous version of Gargantext (v3)
> /!\ (Not maintained any more, release of v4 soon)

- [Current prod version (v3)](https://gargantext.org)
- [Code Source](https://github.com/ISCPIF/gargantext)


